FROM node:lts-alpine as build
WORKDIR /usr/src/app/
COPY ./aggregator-ui/package*.json ./
RUN yarn
COPY ./aggregator-ui .
RUN yarn build

FROM nginx:stable-alpine as run
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]