FROM gradle:6.8.3-jre15 as build
WORKDIR /usr/src/app/worker
COPY ./aggregator-worker ./
RUN ./gradlew clean shadowJar


FROM openjdk:15-jdk-slim as run
WORKDIR /usr/src/app/worker

COPY --from=build /usr/src/app/worker/build/libs ./
COPY ./dockerfiles/worker/entrypoint.sh ./entrypoint.sh 

RUN apt-get update && apt-get -y install -qq cron
RUN chmod +x ./entrypoint.sh

ENTRYPOINT ./entrypoint.sh