#!/bin/bash

echo "Starting worker container..."

declare -p | grep -Ev 'BASHOPTS|BASH_VERSINFO|EUID|PPID|SHELLOPTS|UID' > /container.env

# Setup a cron schedule
echo "
SHELL=/bin/bash
BASH_ENV=/container.env

0 * * * * java -jar /usr/src/app/worker/worker-1.0-all.jar /dev/stdout
" > scheduler.txt

echo "Starting initial sync..."

java -jar /usr/src/app/worker/worker-1.0-all.jar

crontab scheduler.txt
cron -f

