FROM gradle:6.8.3-jre15 as build
WORKDIR /usr/src/app/core
COPY ./aggregator-core ./
RUN ./gradlew clean shadowJar


FROM openjdk:15-jdk-slim as run
WORKDIR /usr/src/app/core

COPY --from=build /usr/src/app/core/build/libs ./
COPY ./dockerfiles/wait-for-it.sh wait-for-it.sh 

RUN chmod +x wait-for-it.sh

EXPOSE 8081

CMD ["./wait-for-it.sh" , "postgres:5432" , "--strict" , "--timeout=300" , "--" , "java", "-jar", "core-1.0-all.jar"]
