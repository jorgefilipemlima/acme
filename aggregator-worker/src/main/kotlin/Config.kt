class Config {
    val baseUrl: String = System.getenv("BASE_URL")
    val apiKey: String = System.getenv("API_KEY")
    val maxRetries: Int = System.getenv("MAX_RETRIES")?.toInt() ?: 5
    val redisUrl: String = System.getenv("REDIS_URL")
}