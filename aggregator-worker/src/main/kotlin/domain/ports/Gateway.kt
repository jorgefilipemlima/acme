package domain.ports

interface Gateway {
    fun getStores(): Sequence<MutableMap<String, String>>
    fun getStoreSeasons(): Sequence<MutableMap<String, String>>
    fun getStoreSpecialFields(): Sequence<MutableMap<String, String>>
}