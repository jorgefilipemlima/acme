package domain.ports

interface Publisher {
    fun publish(topic: String, type: String, messageBody: MutableMap<String, String>)
}