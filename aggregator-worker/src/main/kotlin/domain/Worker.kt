package domain

import domain.ports.Gateway
import domain.ports.Publisher
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private val logger: Logger = LoggerFactory.getLogger(Worker::class.java)

class Worker(private val gateway: Gateway, private val publisher: Publisher) {

    fun work() {
        logger.info("Starting to work...")
        publishAllStores()
        publishAllStoreSeasons()
        publishAllStoreSpecialFields()
    }

    private fun publishAllStores() {
        logger.info("Fetching stores...")
        val stores = this.gateway.getStores()
        logger.info("Publishing stores...")
        publishAllToTopic("stores", stores)
        logger.info("Finished publishing stores...")
    }

    private fun publishAllStoreSeasons() {
        logger.info("Fetching store seasons...")
        val storeSeasons = this.gateway.getStoreSeasons()
        logger.info("Publishing store seasons...")
        publishAllToTopic("store-seasons", storeSeasons)
        logger.info("Finished publishing store seasons...")
    }

    private fun publishAllStoreSpecialFields() {
        logger.info("Fetching store special fields...")
        val storeSpecialFields = this.gateway.getStoreSpecialFields()
        logger.info("Publishing store special fields...")
        publishAllToTopic("store-special-fields", storeSpecialFields)
        logger.info("Finished publishing store special fields...")
    }

    private fun publishAllToTopic(type: String, content: Sequence<MutableMap<String, String>>) {
        content.forEach { entry: MutableMap<String, String> -> publisher.publish("acme", type, entry) }
    }
}