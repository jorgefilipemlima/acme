import adapters.APIGateway
import adapters.RedisPublisher
import domain.Worker
import domain.ports.Gateway
import domain.ports.Publisher
import io.lettuce.core.RedisClient
import java.net.http.HttpClient


fun main(args: Array<String>) {
    val config = Config()
    val httpClient: HttpClient = HttpClient.newBuilder().build()
    val redisClient: RedisClient = RedisClient.create(config.redisUrl)

    redisClient.connect().use {
        val gateway: Gateway = APIGateway(httpClient, config)
        val publisher: Publisher = RedisPublisher(it)

        Worker(gateway, publisher).work()
    }

    redisClient.shutdown()
}