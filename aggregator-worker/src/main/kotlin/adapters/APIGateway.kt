package adapters

import Config
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import domain.ports.Gateway
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.net.http.HttpResponse.BodyHandler
import java.util.concurrent.CompletableFuture

private fun String.toMapList(): List<MutableMap<String, String>> = Gson().fromJson(
    this, object : TypeToken<List<MutableMap<String?, String?>?>>() {}.type
)

class APIGateway(private val httpClient: HttpClient, config: Config) : Gateway {
    private val baseUrl: String = config.baseUrl
    private val apiKey: String = config.apiKey
    private val maxRetries: Int = config.maxRetries

    override fun getStores(): Sequence<MutableMap<String, String>> {
        return sequence {
            var page = 1
            do {
                val response = getStores(page)
                yieldAll(response)
                page++
            } while (response.isNotEmpty())
        }
    }

    private fun getStores(page: Int): List<MutableMap<String, String>> {
        val endpoint = "v1/stores/"
        val handler = HttpResponse.BodyHandlers.ofString()
        val request = HttpRequest.newBuilder()
            .uri(URI.create("$baseUrl/$endpoint?page=$page"))
            .setHeader("apiKey", apiKey)
            .build()

        val response = requestWithRetry(request, handler).get()

        return if (response?.statusCode() == 200) response.body().toMapList() else emptyList()
    }

    override fun getStoreSeasons(): Sequence<MutableMap<String, String>> {
        val endpoint = "other/stores_and_seasons"
        val handler = HttpResponse.BodyHandlers.ofString()
        val request = HttpRequest.newBuilder()
            .uri(URI.create("$baseUrl/$endpoint"))
            .setHeader("apiKey", apiKey)
            .build()

        val response = requestWithRetry(request, handler).get()

        return if (response?.statusCode() == 200) response.body().toMapList().asSequence() else emptySequence()
    }

    override fun getStoreSpecialFields(): Sequence<MutableMap<String, String>> {
        val endpoint = "extra_data.csv"
        val handler = HttpResponse.BodyHandlers.ofString()
        val request = HttpRequest.newBuilder()
            .uri(URI.create("$baseUrl/$endpoint"))
            .setHeader("apiKey", apiKey)
            .build()

        val response = requestWithRetry(request, handler).get()
        return if (response?.statusCode() == 200) response.body()?.let { parseSpecialFieldsFromCSV(it) }
            .orEmpty() else emptySequence()
    }

    private fun parseSpecialFieldsFromCSV(csv: String): Sequence<MutableMap<String, String>> {
        val rows: List<List<String>> = csvReader().readAll(csv)
        val headers = rows[0]

        return sequence {
            rows.slice(1 until rows.size).forEach {
                yield(headers.zip(it).toMap().toMutableMap())
            }
        }
    }

    private fun requestWithRetry(
        request: HttpRequest?,
        handler: BodyHandler<String>?
    ): CompletableFuture<HttpResponse<String>?> {
        return httpClient.sendAsync(request, handler)
            .thenComposeAsync { r -> tryResend(httpClient, request, handler, 1, r) }
    }

    private fun <T> tryResend(
        client: HttpClient, request: HttpRequest?, handler: BodyHandler<T>?,
        count: Int, resp: HttpResponse<T>
    ): CompletableFuture<HttpResponse<T>?>? {
        return if (resp.statusCode() == 200 || count >= maxRetries) {
            CompletableFuture.completedFuture(resp)
        } else {
            client.sendAsync(request, handler)
                .thenComposeAsync { r: HttpResponse<T> ->
                    tryResend(
                        client,
                        request,
                        handler,
                        count + 1,
                        r
                    )
                }
        }
    }
}