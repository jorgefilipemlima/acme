package adapters

import domain.ports.Publisher
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.api.sync.RedisCommands


class RedisPublisher(connection: StatefulRedisConnection<String, String>) : Publisher {
    private val syncCommands: RedisCommands<String, String> = connection.sync()

    override fun publish(topic: String, type: String, messageBody: MutableMap<String, String>) {
        syncCommands.xadd(
            topic,
            messageBody.apply { put("type", type) }
        )
    }
}