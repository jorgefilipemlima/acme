package domain.ports.repositories

import domain.models.Store
import domain.models.StoreDetails

interface StoreRepository : Repository<Store, StoreDetails> {
    fun getByExternalId(externalId: Int): Store?
    fun getAll(page: Int?, name: String?): List<Store>
}
