package domain.ports.repositories

import domain.models.StoreSeason
import domain.models.StoreSeasonDetails

interface StoreSeasonRepository : Repository<StoreSeason, StoreSeasonDetails>
