package domain.ports.repositories

import domain.models.StoreSpecialField
import domain.models.StoreSpecialFieldDetails

interface StoreSpecialFieldRepository : Repository<StoreSpecialField, StoreSpecialFieldDetails>
