package domain.ports.repositories

interface Repository<K, V> {
    fun getById(id: Int): K?
    fun getAll(): List<K>
    fun save(v: V, force: Boolean = false)
    fun saveAll(collection: List<V>)
}
