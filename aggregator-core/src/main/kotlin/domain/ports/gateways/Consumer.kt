package domain.ports.gateways

import domain.usecases.ProcessMessageUseCase

interface Consumer {
    fun consume(topic: String, processMessageUseCase: ProcessMessageUseCase)
}
