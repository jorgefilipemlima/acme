package domain.usecases

class ProcessMessageUseCase(
    private val saveStoreUseCase: SaveStoreUseCase,
    private val saveStoreSeasonUseCase: SaveStoreSeasonUseCase,
    private val saveStoreSpecialFieldUseCase: SaveStoreSpecialFieldUseCase
) {
    fun use(message: MutableMap<String, String>) {
        when (message.remove("type")) {
            "stores" -> saveStoreUseCase.use(message)
            "store-seasons" -> saveStoreSeasonUseCase.use(message)
            "store-special-fields" -> saveStoreSpecialFieldUseCase.use(message)
            else -> println("Could not identify this message. Skipping...")
        }
    }
}
