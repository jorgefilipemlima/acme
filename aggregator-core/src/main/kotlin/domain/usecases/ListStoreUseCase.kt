package domain.usecases

import domain.models.Store
import domain.ports.repositories.StoreRepository

class ListStoreUseCase(private val storeRepository: StoreRepository) {
    fun use(page: Int?, name: String?): List<Store> {
        return storeRepository.getAll(page, name)
    }
}
