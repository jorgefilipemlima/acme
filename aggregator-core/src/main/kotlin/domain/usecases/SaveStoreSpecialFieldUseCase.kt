package domain.usecases

import domain.models.StoreSpecialFieldDetails
import domain.ports.repositories.StoreRepository
import domain.ports.repositories.StoreSpecialFieldRepository
import domain.usecases.exceptions.InvalidArgument
import domain.usecases.exceptions.NonExistingStore

class SaveStoreSpecialFieldUseCase(
    private val storeRepository: StoreRepository,
    private val storeSpecialFieldRepository: StoreSpecialFieldRepository
) {
    fun use(storeSpecialFields: Map<String, String>) {
        val externalId: Int? = runCatching { storeSpecialFields["Store id"]!!.toInt() }.onFailure { throw InvalidArgument() }
            .getOrNull()

        val storeId =
            runCatching { storeRepository.getByExternalId(externalId!!)!!.id }.onFailure { throw NonExistingStore() }
                .getOrNull()

        (storeSpecialFields - "Store id").entries.forEach { e ->
            val storeSpecialField = mapOf("name" to e.key.trim(), "value" to e.value.trim(), "storeId" to storeId.toString())
            val storeSpecialFieldDetails = StoreSpecialFieldDetails.fromMap(storeSpecialField)
            storeSpecialFieldRepository.save(storeSpecialFieldDetails)
        }
    }
}
