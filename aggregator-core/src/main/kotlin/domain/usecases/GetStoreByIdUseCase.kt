package domain.usecases

import domain.models.Store
import domain.ports.repositories.StoreRepository
import domain.usecases.exceptions.NonExistingStore

class GetStoreByIdUseCase(private val storeRepository: StoreRepository) {
    fun use(id: Int): Store {
        return storeRepository.getById(id) ?: throw NonExistingStore()
    }
}
