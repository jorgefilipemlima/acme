package domain.usecases

import domain.models.PatchRequest
import domain.models.StoreDetails
import domain.ports.repositories.StoreRepository
import domain.usecases.exceptions.NonExistingStore

class PatchStoreUseCase(private val storeRepository: StoreRepository) {
    fun use(id: Int, patchRequest: PatchRequest) {
        val store = storeRepository.getById(id) ?: throw NonExistingStore()
        val parsed = parseRequestIntoUpdatedStore(patchRequest, store.toMap())
        storeRepository.save(StoreDetails.fromMap(parsed.toMap()), true)
    }

    private fun parseRequestIntoUpdatedStore(patchRequest: PatchRequest, storeDetails: MutableMap< String, String>): MutableMap<String, String> {
        val propertyToUpdate = patchRequest.path.removePrefix("/")
        if (patchRequest.op == "replace") storeDetails[propertyToUpdate] = patchRequest.value
        storeDetails["id"] = storeDetails["externalId"]!!
        storeDetails["storeType"] = storeDetails["type"]!!

        return storeDetails
    }
}
