package domain.usecases

import domain.models.StoreDetails
import domain.ports.repositories.StoreRepository
import domain.usecases.exceptions.InvalidArgument

class SaveStoreUseCase(private val storeRepository: StoreRepository) {
    fun use(storeData: Map<String, String>) {
        runCatching { StoreDetails.fromMap(storeData) }
            .onFailure { throw InvalidArgument() }
            .onSuccess { storeRepository.save(it) }
    }
}
