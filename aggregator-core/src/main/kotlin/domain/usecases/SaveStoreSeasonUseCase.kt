package domain.usecases

import domain.models.StoreSeasonDetails
import domain.ports.repositories.StoreRepository
import domain.ports.repositories.StoreSeasonRepository
import domain.usecases.exceptions.InvalidArgument
import domain.usecases.exceptions.NonExistingStore

class SaveStoreSeasonUseCase(
    private val storeRepository: StoreRepository,
    private val storeSeasonRepository: StoreSeasonRepository
) {
    fun use(storeSeasonsData: Map<String, String>) {
        val externalId: Int = runCatching { storeSeasonsData["storeId"]!!.toInt() }.onFailure { throw InvalidArgument() }
            .getOrNull()!!

        val storeId =
            runCatching { storeRepository.getByExternalId(externalId)!!.id }.onFailure { throw NonExistingStore() }
                .getOrNull()

        runCatching { mapOf("season" to storeSeasonsData["season"]!!, "storeId" to storeId.toString()) }
            .onFailure { throw InvalidArgument() }
            .onSuccess {
                val storeSeasonDetails = StoreSeasonDetails.fromMap(it)
                return storeSeasonRepository.save(storeSeasonDetails)
            }
    }
}
