package domain.models

data class StoreDetails(
    val externalId: Int,
    val code: String?,
    val description: String?,
    val name: String?,
    val openingDate: String?,
    val type: String
) {
    companion object {
        fun fromMap(map: Map<String, String>): StoreDetails {
            return StoreDetails(
                externalId = map["id"]!!.toInt(),
                code = map["code"]?.trim(),
                description = map["description"]?.trim(),
                name = map["name"]?.trim(),
                openingDate = map["openingDate"]?.trim(),
                type = map["storeType"]?.toUpperCase()?.ifEmpty { "UNKNOWN" } ?: "UNKNOWN"
            )
        }
    }
}
