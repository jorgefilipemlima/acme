package domain.models

enum class StoreType(private val value: String) {
    STORE_FRONT("Store Front"),
    RETAIL("Retail"),
    OTHER("Other"),
    UNKNOWN("Unknown");

    override fun toString(): String {
        return this.value
    }

    companion object {
        fun fromString(type: String?): StoreType {
            return when (type) {
                "STORE FRONT" -> STORE_FRONT
                "RETAIL" -> RETAIL
                "OTHER" -> OTHER
                else -> {
                    UNKNOWN
                }
            }
        }
    }
}
