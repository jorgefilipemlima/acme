package domain.models

private fun String.toSeason(): Pair<String, String> {
    val seasonAndYear = this.split(" ")
    val season = seasonAndYear[0]
    val year = "20${seasonAndYear[1]}"
    return when (season.toUpperCase()) {
        "H1" -> Pair("$year-01-01", "$year-06-30")
        "H2" -> Pair("$year-07-01", "$year-12-31")
        else -> Pair("$year-01-01", "$year-12-31")
    }
}

data class StoreSeasonDetails(
    val storeId: Int,
    val startDate: String,
    val endDate: String
) {
    companion object {
        fun fromMap(map: Map<String, String>): StoreSeasonDetails {
            val (startDate, endDate) = map["season"]!!.toSeason()
            return StoreSeasonDetails(
                storeId = map["storeId"]!!.toInt(),
                startDate = startDate,
                endDate = endDate,
            )
        }
    }
}
