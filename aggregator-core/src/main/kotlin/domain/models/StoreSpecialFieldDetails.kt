package domain.models

data class StoreSpecialFieldDetails(
    val storeId: Int,
    val name: String,
    val value: String,
) {
    companion object {
        fun fromMap(map: Map<String, String>): StoreSpecialFieldDetails {
            return StoreSpecialFieldDetails(
                storeId = map["storeId"]!!.toInt(),
                name = map["name"]!!.trim(),
                value = map["value"]!!.trim(),
            )
        }
    }
}
