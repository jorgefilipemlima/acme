package domain.models

import kotlinx.serialization.Serializable

@Serializable
data class Store(
    val id: Int?,
    val externalId: Int?,
    val code: String?,
    val description: String?,
    val name: String?,
    val openingDate: String?,
    val type: StoreType,
    val specialFields: List<StoreSpecialField>,
    val seasons: List<StoreSeason>,
) {
    fun toMap(): MutableMap<String, String> = mutableMapOf(
        "externalId" to externalId!!.toString(),
        "code" to code!!,
        "description" to description!!,
        "name" to name!!,
        "openingDate" to openingDate!!,
        "type" to type.toString(),
    )
}
