package domain.models

import kotlinx.serialization.Serializable

@Serializable
data class PatchRequest(
    val op: String,
    val path: String,
    val value: String
)
