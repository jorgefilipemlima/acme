package domain.models

import kotlinx.serialization.Serializable

@Serializable
data class StoreSpecialField(
    val name: String,
    val value: String
)
