package domain.models

import kotlinx.serialization.Serializable

@Serializable
data class StoreSeason(
    val startDate: String,
    val endDate: String
)
