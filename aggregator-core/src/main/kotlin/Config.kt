class Config {
    val port: Int = System.getenv("PORT")!!.toInt()
    val redisUrl: String = System.getenv("REDIS_URL")
    val databaseUrl: String = System.getenv("DATABASE_URL")
    val databaseUser: String = System.getenv("DATABASE_USER")
    val databasePassword: String = System.getenv("DATABASE_PASSWORD")
}
