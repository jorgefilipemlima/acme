package adapters.gateways

import domain.ports.gateways.Consumer
import domain.usecases.ProcessMessageUseCase
import io.lettuce.core.RedisBusyException
import io.lettuce.core.XGroupCreateArgs
import io.lettuce.core.XReadArgs
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.api.sync.RedisCommands
import io.lettuce.core.Consumer as RConsumer

class RedisConsumer(connection: StatefulRedisConnection<String, String>) : Consumer {
    private val syncCommands: RedisCommands<String, String> = connection.sync()

    override fun consume(topic: String, processMessageUseCase: ProcessMessageUseCase) {
        setupGroup(topic)
        keepConsuming(topic, processMessageUseCase)
    }

    private fun setupGroup(topic: String) {
        try {
            syncCommands.xgroupCreate(
                XReadArgs.StreamOffset.from(topic, "0-0"),
                "acme_1",
                XGroupCreateArgs().mkstream(true)
            )
        } catch (redisBusyException: RedisBusyException) {
            println(String.format("\t Group '%s' already exists", "acme_1"))
        }
    }

    private fun keepConsuming(topic: String, processMessageUseCase: ProcessMessageUseCase) {
        val failed = mutableMapOf("stores" to 0, "store-seasons" to 0, "store-special-fields" to 0)
        while (true) {
            val messages = syncCommands.xreadgroup(
                RConsumer.from("acme_1", "core_1"),
                XReadArgs.StreamOffset.lastConsumed(topic)
            )
            if (messages.isNotEmpty()) {
                messages.forEach {
                    val type = it.body["type"]!!
                    try {
                        processMessageUseCase.use(it.body)
                        syncCommands.xack(topic, "acme_1", it.id)
                    } catch (e: Exception) {
                        failed[type] = failed[type]!!.plus(1)
                    } catch (e: Throwable) {
                        failed[type] = failed[type]!!.plus(1)
                    }
                }
                println("Failed to Process $failed")
            }
        }
    }
}
