package adapters.repositories.pg

import adapters.repositories.pg.entities.StoreSeasons
import adapters.repositories.pg.entities.StoreSpecialField
import adapters.repositories.pg.entities.StoreSpecialFields
import adapters.repositories.pg.entities.Stores
import domain.models.StoreSpecialFieldDetails
import domain.ports.repositories.StoreSpecialFieldRepository
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import domain.models.StoreSpecialField as StoreSpecialFieldModel

private fun StoreSpecialField.toModel() = StoreSpecialFieldModel(
    name = this.name,
    value = this.value
)

class StoreSpecialFieldRepository(private val database: Database) : StoreSpecialFieldRepository {
    init {
        transaction(database) {
            SchemaUtils.createMissingTablesAndColumns(
                Stores,
                StoreSeasons,
                StoreSpecialFields
            )
        }
    }

    override fun getById(id: Int): StoreSpecialFieldModel? {
        TODO("Not yet implemented")
    }

    override fun getAll(): List<StoreSpecialFieldModel> = transaction(database) {
        StoreSpecialField.all().map { it.toModel() }
    }

    override fun save(v: StoreSpecialFieldDetails, force: Boolean): Unit = transaction(database) {
        if (StoreSpecialField.find {
            StoreSpecialFields.name eq v.name
            StoreSpecialFields.store eq v.storeId
        }.firstOrNull() == null
        ) {
            StoreSpecialFields.insert {
                it[name] = v.name
                it[value] = v.value
                it[store] = EntityID(v.storeId, Stores)
            }[StoreSpecialFields.id].value
        } else {
            StoreSpecialFields.update({
                StoreSpecialFields.name eq v.name
                StoreSpecialFields.store eq v.storeId
            }) {
                it[name] = v.name
                it[value] = v.value
                it[store] = EntityID(v.storeId, Stores)
            }
        }
    }

    override fun saveAll(collection: List<StoreSpecialFieldDetails>) {
        TODO("Not yet implemented")
    }
}
