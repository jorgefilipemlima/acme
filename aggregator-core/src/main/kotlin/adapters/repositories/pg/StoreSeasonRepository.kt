package adapters.repositories.pg

import adapters.repositories.pg.entities.StoreSeason
import adapters.repositories.pg.entities.StoreSeasons
import adapters.repositories.pg.entities.StoreSpecialFields
import adapters.repositories.pg.entities.Stores
import domain.models.StoreSeasonDetails
import domain.ports.repositories.StoreSeasonRepository
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import domain.models.StoreSeason as StoreSeasonModel

private fun StoreSeason.toModel() = StoreSeasonModel(
    startDate = this.startDate,
    endDate = this.endDate
)

class StoreSeasonRepository(private val database: Database) : StoreSeasonRepository {
    init {
        transaction(database) {
            SchemaUtils.createMissingTablesAndColumns(
                Stores,
                StoreSeasons,
                StoreSpecialFields
            )
        }
    }

    override fun getById(id: Int): StoreSeasonModel? {
        TODO("Not yet implemented")
    }

    override fun getAll(): List<StoreSeasonModel> = transaction(database) {
        StoreSeason.all().map { it.toModel() }
    }

    override fun save(v: StoreSeasonDetails, force: Boolean): Unit = transaction(database) {
        if (StoreSeason.find {
            StoreSeasons.startDate eq v.startDate
            StoreSeasons.endDate eq v.endDate
            StoreSeasons.store eq v.storeId
        }.firstOrNull() == null
        ) {
            StoreSeasons.insert {
                it[startDate] = v.startDate
                it[endDate] = v.endDate
                it[store] = EntityID(v.storeId, Stores)
            }[StoreSeasons.id].value
        } else {
            StoreSeasons.update({
                StoreSeasons.startDate eq v.startDate
                StoreSeasons.endDate eq v.endDate
                StoreSeasons.store eq v.storeId
            }) {
                it[startDate] = v.startDate
                it[endDate] = v.endDate
                it[store] = EntityID(v.storeId, Stores)
            }
        }
    }

    override fun saveAll(collection: List<StoreSeasonDetails>) = transaction(database) {
        TODO("Not yet implemented")
    }
}
