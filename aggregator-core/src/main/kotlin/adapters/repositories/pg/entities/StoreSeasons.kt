package adapters.repositories.pg.entities

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption.CASCADE

object StoreSeasons : IntIdTable("store_seasons") {
    val startDate = varchar("start_date", length = 15)
    val endDate = varchar("end_date", length = 15)
    val store = reference("store", Stores, onDelete = CASCADE)
    init {
        index(true, startDate, endDate, store)
    }
}

class StoreSeason(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<StoreSeason>(StoreSeasons)
    var startDate by StoreSeasons.startDate
    var endDate by StoreSeasons.endDate
    var store by Store referencedOn StoreSeasons.store
}
