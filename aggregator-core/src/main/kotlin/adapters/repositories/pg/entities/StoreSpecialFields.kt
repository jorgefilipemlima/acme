package adapters.repositories.pg.entities

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption.CASCADE

object StoreSpecialFields : IntIdTable("store_special_fields") {
    val name = varchar("name", length = 1000)
    val value = varchar("value", length = 1000)
    val store = reference("store", Stores, onDelete = CASCADE)
}

class StoreSpecialField(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<StoreSpecialField>(StoreSpecialFields)
    var name by StoreSpecialFields.name
    var value by StoreSpecialFields.value
    var store by Store referencedOn StoreSpecialFields.store
}
