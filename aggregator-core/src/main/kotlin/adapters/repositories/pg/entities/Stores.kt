package adapters.repositories.pg.entities

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Stores : IntIdTable("stores") {
    val externalId = integer("external_id").uniqueIndex()
    val code = varchar("code", length = 50).nullable()
    val description = varchar("description", length = 3000).nullable()
    val name = varchar("name", length = 50).nullable()
    val openingDate = varchar("opening_date", length = 15).nullable()
    val type = varchar("store_type", length = 15).nullable()
    val dirty = bool("dirty").default(false)
}

class Store(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Store>(Stores)
    var externalId by Stores.externalId
    var code by Stores.code
    var description by Stores.description
    var name by Stores.name
    var openingDate by Stores.openingDate
    var type by Stores.type
    var dirty by Stores.dirty
    val storeSeasons by StoreSeason referrersOn StoreSeasons.store
    val specialFields by StoreSpecialField referrersOn StoreSpecialFields.store
}
