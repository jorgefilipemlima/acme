package adapters.repositories.pg

import adapters.repositories.pg.entities.Store
import adapters.repositories.pg.entities.StoreSeasons
import adapters.repositories.pg.entities.StoreSpecialFields
import adapters.repositories.pg.entities.Stores
import domain.models.StoreDetails
import domain.models.StoreSeason
import domain.models.StoreSpecialField
import domain.models.StoreType
import domain.ports.repositories.StoreRepository
import org.jetbrains.exposed.dao.with
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import domain.models.Store as StoreModel

private fun Store.toModel() = StoreModel(
    id = this.id.value,
    externalId = this.externalId,
    code = this.code,
    description = this.description,
    name = this.name,
    openingDate = this.openingDate,
    type = StoreType.fromString(this.type),
    specialFields = this.specialFields.map { sf -> StoreSpecialField(sf.name, sf.value) },
    seasons = this.storeSeasons.map { ss -> StoreSeason(ss.startDate, ss.endDate) },
)

class StoreRepository(private val database: Database) : StoreRepository {
    init {
        transaction(database) {
            SchemaUtils.createMissingTablesAndColumns(
                Stores,
                StoreSeasons,
                StoreSpecialFields
            )
        }
    }

    override fun getById(id: Int): StoreModel? = transaction(database) {
        Store.findById(id)?.toModel()
    }

    override fun getAll(): List<domain.models.Store> {
        TODO("Not yet implemented")
    }

    override fun getByExternalId(externalId: Int): StoreModel? = transaction(database) {
        Store.find { Stores.externalId eq externalId }.firstOrNull()?.toModel()
    }

    override fun getAll(page: Int?, name: String?): List<StoreModel> = transaction(database) {
        var query: SizedIterable<Store> = Store.all()

        if (name != null) query = Store.find { Stores.name.lowerCase() like "%$name%".toLowerCase() }
        if (page != null) query = query.limit(10, (page - 1) * 10)

        query.orderBy(Stores.id to SortOrder.ASC).with(Store::storeSeasons, Store::specialFields).map {
            it.toModel()
        }
    }

    override fun save(v: StoreDetails, force: Boolean): Unit = transaction(database) {
        val existingStore = Store.find { Stores.externalId eq v.externalId }.singleOrNull()
        if (existingStore == null) {
            Stores.insert {
                it[externalId] = v.externalId
                it[code] = v.code
                it[description] = v.description
                it[name] = v.name
                it[openingDate] = v.openingDate
                it[type] = v.type
            }[Stores.id].value
        } else {
            Stores.update({ Stores.externalId eq v.externalId }) {
                it[externalId] = v.externalId
                it[code] = v.code
                it[description] = v.description
                it[openingDate] = v.openingDate
                it[type] = v.type
                if (force) it[dirty] = true
                if (!existingStore.dirty || force) it[name] = v.name
            }
        }
    }

    override fun saveAll(collection: List<StoreDetails>): Unit = transaction(database) {
        TODO("Not yet implemented")
    }
}
