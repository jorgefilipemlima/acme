package adapters.api.http

import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import domain.usecases.ListStoreUseCase
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

class ExportStoresAsCSVHandler(private val listStoreUseCase: ListStoreUseCase) {
    suspend fun handle(call: ApplicationCall) {
        val stores = listStoreUseCase.use(null, null)
        val headers = listOf("id", "type", "name", "code", "openingDate", "description", "seasons", "specialFields")

        call.response.header(
            HttpHeaders.ContentDisposition,
            ContentDisposition.Attachment.withParameter(ContentDisposition.Parameters.FileName, "export.csv")
                .toString()
        )
        call.respondOutputStream {
            csvWriter().writeAll(
                listOf(headers) +
                    stores.map {
                        listOf(
                            it.externalId,
                            it.type,
                            it.name,
                            it.code,
                            it.openingDate,
                            it.description,
                            it.seasons.joinToString(prefix = "[", postfix = "]") { s -> "${s.startDate} - ${s.endDate}" },
                            it.specialFields.joinToString(prefix = "[", postfix = "]") { sf -> "{${sf.name}: ${sf.value}}" },
                        )
                    },
                this
            )
        }
    }
}
