package adapters.api.http

import domain.usecases.ListStoreUseCase
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

class GetStoresHandler(private val listStoreUseCase: ListStoreUseCase) {
    suspend fun handle(call: ApplicationCall) {
        try {
            val page: Int? = call.parameters["page"]?.toInt()
            val query: String? = call.parameters["q"]?.let { it.ifEmpty { null } }
            val stores = listStoreUseCase.use(page, query)
            call.respond(stores)
        } catch (e: NumberFormatException) {
            call.respond(HttpStatusCode.BadRequest, "Page must be an integer")
        }
    }
}
