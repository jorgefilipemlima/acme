package adapters.api.http

import domain.usecases.GetStoreByIdUseCase
import domain.usecases.ListStoreUseCase
import domain.usecases.PatchStoreUseCase
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

class HttpAPI(
    private val port: Int,
    private val listStoreUseCase: ListStoreUseCase,
    private val getStoreByIdUseCase: GetStoreByIdUseCase,
    private val patchStoreUseCase: PatchStoreUseCase
) {
    fun run() = embeddedServer(Netty, port = port) {
        installJSON()
        installCORS()
        routing {
            registerStoreRoutes()
        }
    }.start(wait = true)

    private fun Application.installCORS() {
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Patch)
            header(HttpHeaders.Authorization)
            allowCredentials = true
            allowNonSimpleContentTypes = true
            anyHost()
        }
    }

    private fun Application.installJSON() {
        install(ContentNegotiation) {
            jackson()
        }
    }

    private fun Routing.registerStoreRoutes() {
        route("/api") {
            route("/stores") {
                get {
                    GetStoresHandler(listStoreUseCase).handle(call)
                }
                get("{id}") {
                    GetStoreByIdHandler(getStoreByIdUseCase).handle(call)
                }
                patch("{id}") {
                    PatchStoreHandler(patchStoreUseCase).handle(call)
                }

                route("/export.csv") {
                    get {
                        ExportStoresAsCSVHandler(listStoreUseCase).handle(call)
                    }
                }
            }
        }
    }
}
