package adapters.api.http

import domain.usecases.GetStoreByIdUseCase
import domain.usecases.exceptions.NonExistingStore
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

class GetStoreByIdHandler(private val getStoreByIdUseCase: GetStoreByIdUseCase) {
    suspend fun handle(call: ApplicationCall) {
        try {
            val id: Int? = call.parameters["id"]?.toInt()
            val store = getStoreByIdUseCase.use(id!!)
            call.respond(store)
        } catch (e: NonExistingStore) {
            call.respond(HttpStatusCode.NotFound, "Store not found.")
        } catch (e: NumberFormatException) {
            call.respond(HttpStatusCode.BadRequest, "Id must be an integer")
        }
    }
}
