package adapters.api.http

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import domain.models.PatchRequest
import domain.usecases.PatchStoreUseCase
import domain.usecases.exceptions.NonExistingStore
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*

class PatchStoreHandler(private val patchStoreUseCase: PatchStoreUseCase) {
    suspend fun handle(call: ApplicationCall) {
        try {
            val patchRequest = call.receive<PatchRequest>()
            val id: Int = call.parameters["id"]!!.toInt()
            patchStoreUseCase.use(id, patchRequest)
            call.respond(HttpStatusCode.NoContent)
        } catch (e: NonExistingStore) {
            call.respond(HttpStatusCode.NotFound, "Store not found.")
        } catch (e: JsonParseException) {
            call.respond(HttpStatusCode.BadRequest, "Invalid patch request.")
        } catch (e: MissingKotlinParameterException) {
            call.respond(HttpStatusCode.BadRequest, "Invalid patch request.")
        }
    }
}
