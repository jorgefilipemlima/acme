package adapters.api.stream

import adapters.gateways.RedisConsumer
import domain.ports.gateways.Consumer
import domain.usecases.ProcessMessageUseCase
import io.lettuce.core.RedisClient

class StreamAPI(
    private val redisClient: RedisClient,
    private val processMessageUseCase: ProcessMessageUseCase
) {
    fun run() = redisClient.connect().use {
        val consumer: Consumer = RedisConsumer(it)
        consumer.consume("acme", processMessageUseCase)
    }
}
