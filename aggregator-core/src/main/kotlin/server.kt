import adapters.api.http.HttpAPI
import adapters.api.stream.StreamAPI
import domain.ports.repositories.StoreRepository
import domain.ports.repositories.StoreSeasonRepository
import domain.ports.repositories.StoreSpecialFieldRepository
import domain.usecases.*
import io.lettuce.core.RedisClient
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.exposed.sql.Database
import adapters.repositories.pg.StoreRepository as PostgresStoreRepository
import adapters.repositories.pg.StoreSeasonRepository as PostgresStoreSeasonRepository
import adapters.repositories.pg.StoreSpecialFieldRepository as PostgresStoreSpecialFieldRepository

fun main() {
    val config = Config()
    val redisClient: RedisClient = RedisClient.create(config.redisUrl)
    val database = Database.connect(
        driver = "org.postgresql.Driver",
        url = config.databaseUrl,
        user = config.databaseUser,
        password = config.databasePassword,
    )

    val storeRepository: StoreRepository = PostgresStoreRepository(database)
    val storeSeasonRepository: StoreSeasonRepository = PostgresStoreSeasonRepository(database)
    val storeSpecialFieldRepository: StoreSpecialFieldRepository = PostgresStoreSpecialFieldRepository(database)

    val listStoreUseCase = ListStoreUseCase(storeRepository)
    val getStoreByIdUseCase = GetStoreByIdUseCase(storeRepository)
    val saveStoreUseCase = SaveStoreUseCase(storeRepository)
    val patchStoreUseCase = PatchStoreUseCase(storeRepository)
    val saveStoreSeasonUseCase = SaveStoreSeasonUseCase(storeRepository, storeSeasonRepository)
    val saveStoreSpecialFieldUseCase = SaveStoreSpecialFieldUseCase(storeRepository, storeSpecialFieldRepository)
    val processMessageUseCase =
        ProcessMessageUseCase(saveStoreUseCase, saveStoreSeasonUseCase, saveStoreSpecialFieldUseCase)

    GlobalScope.launch {
        HttpAPI(config.port, listStoreUseCase, getStoreByIdUseCase, patchStoreUseCase).run()
    }
    GlobalScope.launch {
        StreamAPI(redisClient, processMessageUseCase).run()
    }
}
