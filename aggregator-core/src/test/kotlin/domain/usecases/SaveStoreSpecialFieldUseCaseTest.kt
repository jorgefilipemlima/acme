package domain.usecases

import domain.models.Store
import domain.models.StoreSpecialFieldDetails
import domain.models.StoreType
import domain.ports.repositories.StoreRepository
import domain.ports.repositories.StoreSpecialFieldRepository
import domain.usecases.exceptions.InvalidArgument
import domain.usecases.exceptions.NonExistingStore
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.times

class SaveStoreSpecialFieldUseCaseTest {
    private val mockStoreRepository = Mockito.mock(StoreRepository::class.java)
    private val mockStoreSpecialFieldRepository = Mockito.mock(StoreSpecialFieldRepository::class.java)

    @Test
    fun `given it receives valid params it saves the store special field`() {
        val store = Store(2, 1, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getByExternalId(1))
            .thenReturn(store)

        val storeSpecialField = mapOf(
            "store id" to "1",
            "special field 1" to "sf1",
            "special field 2" to "sf2",
        )

        SaveStoreSpecialFieldUseCase(mockStoreRepository, mockStoreSpecialFieldRepository).use(storeSpecialField)

        Mockito.verify(mockStoreSpecialFieldRepository).save(StoreSpecialFieldDetails(2, "special field 1", "sf1"))
        Mockito.verify(mockStoreSpecialFieldRepository).save(StoreSpecialFieldDetails(2, "special field 2", "sf2"))
    }

    @Test
    fun `given no existing store it throws`() {
        val store = Store(1, 2, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getByExternalId(2))
            .thenReturn(store)

        val storeSpecialField = mapOf(
            "store id" to "1",
            "special field 1" to "sf1",
            "special field 2" to "sf2",
        )

        assertThrows<NonExistingStore> {
            SaveStoreSpecialFieldUseCase(mockStoreRepository, mockStoreSpecialFieldRepository).use(storeSpecialField)
        }
    }

    @Test
    fun `given invalid params it throws`() {
        val store = Store(1, 2, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getByExternalId(2))
            .thenReturn(store)

        val storeSpecialField = mapOf(
            "storeid" to "2",
        )

        assertThrows<InvalidArgument> {
            SaveStoreSpecialFieldUseCase(mockStoreRepository, mockStoreSpecialFieldRepository).use(storeSpecialField)
        }
    }
}
