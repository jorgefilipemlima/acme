package domain.usecases

import domain.models.Store
import domain.models.StoreType
import domain.ports.repositories.StoreRepository
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify
import kotlin.test.assertEquals

class ListStoreUseCaseTest {
    private val mockStoreRepository = Mockito.mock(StoreRepository::class.java)

    @Test
    fun `should return a list of existing stores`() {
        val store = Store(
            1,
            2,
            "random_code",
            "some_description",
            "some_name",
            "2021-02-28",
            StoreType.STORE_FRONT,
            listOf(),
            listOf()
        )
        Mockito.`when`(mockStoreRepository.getAll(1, null)).thenReturn(listOf(store))

        val stores = ListStoreUseCase(mockStoreRepository).use(page = 1, name = null)

        assertEquals(listOf(store), stores)
        verify(mockStoreRepository).getAll(1, null)
    }
}
