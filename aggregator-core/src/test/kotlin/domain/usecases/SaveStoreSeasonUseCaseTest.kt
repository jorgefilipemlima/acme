package domain.usecases

import domain.models.Store
import domain.models.StoreSeasonDetails
import domain.models.StoreType
import domain.ports.repositories.StoreRepository
import domain.ports.repositories.StoreSeasonRepository
import domain.usecases.exceptions.InvalidArgument
import domain.usecases.exceptions.NonExistingStore
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito

class SaveStoreSeasonUseCaseTest {
    private val mockStoreRepository = Mockito.mock(StoreRepository::class.java)
    private val mockStoreSeasonRepository = Mockito.mock(StoreSeasonRepository::class.java)

    @Test
    fun `given it receives valid params it saves the store season`() {
        val store = Store(2, 1, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getByExternalId(1))
            .thenReturn(store)

        val storeSeason = mapOf(
            "storeId" to "1",
            "season" to "H1 22",
        )

        SaveStoreSeasonUseCase(mockStoreRepository, mockStoreSeasonRepository).use(storeSeason)

        Mockito.verify(mockStoreSeasonRepository).save(StoreSeasonDetails(2, "2022-01-01", "2022-06-30"))
    }

    @Test
    fun `given no existing store it throws`() {
        val store = Store(1, 2, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getByExternalId(2))
            .thenReturn(store)

        val storeSeason = mapOf(
            "storeId" to "1",
            "season" to "H1 22",
        )

        assertThrows<NonExistingStore> {
            SaveStoreSeasonUseCase(mockStoreRepository, mockStoreSeasonRepository).use(storeSeason)
        }
    }

    @Test
    fun `given invalid params it throws`() {
        val store = Store(1, 2, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getByExternalId(2))
            .thenReturn(store)

        assertThrows<InvalidArgument> {
            SaveStoreSeasonUseCase(mockStoreRepository, mockStoreSeasonRepository).use(
                mapOf(
                    "storeId" to "2",
                )
            )
        }

        assertThrows<InvalidArgument> {
            SaveStoreSeasonUseCase(mockStoreRepository, mockStoreSeasonRepository).use(
                mapOf(
                    "storead" to "2",
                )
            )
        }
    }
}
