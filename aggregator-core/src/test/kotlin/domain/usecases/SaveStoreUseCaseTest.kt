package domain.usecases

import domain.models.StoreDetails
import domain.ports.repositories.StoreRepository
import domain.usecases.exceptions.InvalidArgument
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.verify

class SaveStoreUseCaseTest {
    private val mockStoreRepository = Mockito.mock(StoreRepository::class.java)

    @Test
    fun `given it receives valid params`() {
        val storeDetails = mapOf(
            "id" to "1",
            "code" to "random_code",
            "description" to "random_description",
            "name" to "random_name",
            "openingDate" to "2021-03-06",
            "storeType" to "OTHER",
        )

        SaveStoreUseCase(mockStoreRepository).use(storeDetails)

        verify(mockStoreRepository).save(
            StoreDetails(
                1,
                "random_code",
                "random_description",
                "random_name",
                "2021-03-06",
                "OTHER"
            )
        )
    }

    @Test
    fun `given the map has unknown store type, the type is set to unknown`() {
        val storeDetails = mapOf(
            "id" to "1",
            "code" to "random_code",
            "description" to "random_description",
            "name" to "random_name",
            "openingDate" to "2021-03-06",
            "storeType" to "UNKNOWN",
        )

        SaveStoreUseCase(mockStoreRepository).use(storeDetails)

        verify(mockStoreRepository).save(
            StoreDetails(
                1,
                "random_code",
                "random_description",
                "random_name",
                "2021-03-06",
                "UNKNOWN"
            )
        )
    }

    @Test
    fun `given the map is missing the id it throws`() {
        val storeDetails = mapOf(
            "code" to "random_code",
            "description" to "random_description",
            "name" to "random_name",
            "openingDate" to "2021-03-06",
            "storeType" to "OTHER",
        )

        assertThrows<InvalidArgument> { SaveStoreUseCase(mockStoreRepository).use(storeDetails) }
    }
}
