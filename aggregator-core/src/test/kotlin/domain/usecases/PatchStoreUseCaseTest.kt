package domain.usecases

import domain.models.*
import domain.ports.repositories.StoreRepository
import domain.usecases.exceptions.NonExistingStore
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito

class PatchStoreUseCaseTest {
    private val mockStoreRepository = Mockito.mock(StoreRepository::class.java)

    @Test
    fun `given it receives valid params it saves the store`() {
        val store = Store(2, 1, "", "", "", "", StoreType.OTHER, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getById(2))
            .thenReturn(store)

        val request = PatchRequest("replace", "/name", "newName")

        PatchStoreUseCase(mockStoreRepository).use(2, request)

        Mockito.verify(mockStoreRepository).save(StoreDetails(1, "", "", "newName", "", "OTHER"), true)
    }

    @Test
    fun `given no existing store it throws`() {
        val store = Store(1, 2, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getById(1))
            .thenReturn(store)

        val request = PatchRequest("replace", "/name", "newName")

        assertThrows<NonExistingStore> {
            PatchStoreUseCase(mockStoreRepository).use(2, request)
        }
    }
}
