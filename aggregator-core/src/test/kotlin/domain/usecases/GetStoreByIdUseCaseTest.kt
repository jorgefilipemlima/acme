package domain.usecases

import domain.models.Store
import domain.models.StoreType
import domain.ports.repositories.StoreRepository
import domain.usecases.exceptions.NonExistingStore
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import kotlin.test.assertEquals

class GetStoreByIdUseCaseTest {
    private val mockStoreRepository = Mockito.mock(StoreRepository::class.java)

    @Test
    fun `given store exists it is returned`() {
        val store = Store(2, 1, "", "", "", "", StoreType.STORE_FRONT, emptyList(), emptyList())

        Mockito.`when`(mockStoreRepository.getById(2))
            .thenReturn(store)

        val result = GetStoreByIdUseCase(mockStoreRepository).use(2)

        assertEquals(2, result.id)
        assertEquals(1, result.externalId)
    }

    @Test
    fun `give store does not exist it throws`() {

        Mockito.`when`(mockStoreRepository.getById(2))
            .thenReturn(null)

        assertThrows<NonExistingStore> {
            GetStoreByIdUseCase(mockStoreRepository).use(2)
        }
    }
}
