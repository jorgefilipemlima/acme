package domain.usecases

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class ProcessMessageUseCaseTest {
    private val mockSaveStoreUseCase = Mockito.mock(SaveStoreUseCase::class.java)
    private val mockSaveStoreSeasonUseCase = Mockito.mock(SaveStoreSeasonUseCase::class.java)
    private val mockSaveStoreSpecialFieldUseCase = Mockito.mock(SaveStoreSpecialFieldUseCase::class.java)

    @Test
    fun `uses save store use case when type is stores`() {
        val message = mutableMapOf(
            "type" to "stores",
        )

        ProcessMessageUseCase(
            mockSaveStoreUseCase,
            mockSaveStoreSeasonUseCase,
            mockSaveStoreSpecialFieldUseCase
        ).use(message)

        Mockito.verify(mockSaveStoreUseCase, times(1)).use(message)
        Mockito.verify(mockSaveStoreSeasonUseCase, times(0)).use(message)
        Mockito.verify(mockSaveStoreSpecialFieldUseCase, times(0)).use(message)
    }

    @Test
    fun `uses save store season use case when type is store-seasons`() {
        val message = mutableMapOf(
            "type" to "store-seasons",
        )

        ProcessMessageUseCase(
            mockSaveStoreUseCase,
            mockSaveStoreSeasonUseCase,
            mockSaveStoreSpecialFieldUseCase
        ).use(message)

        Mockito.verify(mockSaveStoreUseCase, times(0)).use(message)
        Mockito.verify(mockSaveStoreSeasonUseCase, times(1)).use(message)
        Mockito.verify(mockSaveStoreSpecialFieldUseCase, times(0)).use(message)
    }

    @Test
    fun `uses save store use case when type is store-special-fields`() {
        val message = mutableMapOf(
            "type" to "store-special-fields",
        )

        ProcessMessageUseCase(
            mockSaveStoreUseCase,
            mockSaveStoreSeasonUseCase,
            mockSaveStoreSpecialFieldUseCase
        ).use(message)

        Mockito.verify(mockSaveStoreUseCase, times(0)).use(message)
        Mockito.verify(mockSaveStoreSeasonUseCase, times(0)).use(message)
        Mockito.verify(mockSaveStoreSpecialFieldUseCase, times(1)).use(message)
    }

    @Test
    fun `does nothing when type not recognized`() {
        val message = mutableMapOf(
            "type" to "random",
        )

        ProcessMessageUseCase(
            mockSaveStoreUseCase,
            mockSaveStoreSeasonUseCase,
            mockSaveStoreSpecialFieldUseCase
        ).use(message)

        Mockito.verify(mockSaveStoreUseCase, times(0)).use(message)
        Mockito.verify(mockSaveStoreSeasonUseCase, times(0)).use(message)
        Mockito.verify(mockSaveStoreSpecialFieldUseCase, times(0)).use(message)
    }
}
