package adapters.api

import adapters.api.http.HttpAPI
import domain.models.StoreDetails
import domain.ports.repositories.StoreRepository
import domain.usecases.GetStoreByIdUseCase
import domain.usecases.ListStoreUseCase
import domain.usecases.PatchStoreUseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.skyscreamer.jsonassert.JSONAssert
import org.testcontainers.containers.PostgreSQLContainer
import java.net.URI
import java.net.http.HttpClient.newHttpClient
import java.net.http.HttpRequest.newBuilder
import kotlin.test.assertEquals
import adapters.repositories.pg.StoreRepository as PostgresStoreRepository
import java.net.http.HttpRequest.BodyPublishers.ofString as ofStringPublisher
import java.net.http.HttpResponse.BodyHandlers.ofString as ofStringHandler

@TestInstance(Lifecycle.PER_CLASS)
class HttpAPITest {
    private val postgres = PostgreSQLContainer<Nothing>("postgres").apply {
        withDatabaseName("acme")
        withExposedPorts(5434)
        withUsername("y")
        withPassword("z")
    }

    private val repository: StoreRepository = run {
        postgres.start()
        PostgresStoreRepository(
            Database.connect(
                driver = "org.postgresql.Driver",
                url = postgres.jdbcUrl,
                user = postgres.username,
                password = postgres.password,
            )
        ).apply {
            this.save(StoreDetails(1, "code", "description", "name", "2021-03-06", "OTHER"))
        }
    }

    @BeforeAll
    fun `setup api`() {
        GlobalScope.launch {
            HttpAPI(8081, ListStoreUseCase(repository), GetStoreByIdUseCase(repository), PatchStoreUseCase(repository)).run()
        }
    }

    @BeforeAll
    fun `wait for api to be up`() {
        // Ugh, dirty hack in order for the api to be up, this should totally be removed and improved
        Thread.sleep(7_000)
    }

    @Test
    fun `when get single store returns 200 and the store when existing`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET()
                .uri(URI("http://localhost:8081/api/stores/1"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.OK_200, response.statusCode())
        JSONAssert.assertEquals(
            """
                        {
                          "id": 1,
                          "externalId": 1,
                          "code": "code",
                          "description": "description",
                          "name": "name",
                          "openingDate": "2021-03-06",
                          "type": "OTHER",
                          "specialFields": [],
                          "seasons": []
                        }
                    """,
            response.body(),
            true
        )
    }

    @Test
    fun `when get single store returns 404 when store does not exist`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET()
                .uri(URI("http://localhost:8081/api/stores/2"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.NOT_FOUND_404, response.statusCode())
    }

    @Test
    fun `when get single store returns 400 when given id is not a Int`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET()
                .uri(URI("http://localhost:8081/api/stores/a"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.BAD_REQUEST_400, response.statusCode())
    }

    @Test
    fun `when get all stores by page returns 200 and the stores`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET()
                .uri(URI("http://localhost:8081/api/stores?page=1"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.OK_200, response.statusCode())
        JSONAssert.assertEquals(
            """
                        [{
                          "id": 1,
                          "externalId": 1,
                          "code": "code",
                          "description": "description",
                          "name": "name",
                          "openingDate": "2021-03-06",
                          "type": "OTHER",
                          "specialFields": [],
                          "seasons": []
                        }]
                    """,
            response.body(),
            true
        )
    }

    @Test
    fun `when get all stores by name returns 200 and empty list when no match`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET()
                .uri(URI("http://localhost:8081/api/stores?page=1&q=random"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.OK_200, response.statusCode())
        JSONAssert.assertEquals(
            """
                        []
                    """,
            response.body(),
            true
        )
    }

    @Test
    fun `when get all stores by page returns 400 when page is not an Int`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET().uri(URI("http://localhost:8081/api/stores?page=a"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.BAD_REQUEST_400, response.statusCode())
    }

    @Test
    fun `when patching a store returns 204 when store exists`() {
        val payload = """
            {
                "op": "replace",
                "path": "/name",
                "value": "name"
            }
        """

        val response = newHttpClient().send(
            newBuilder()
                .method("PATCH", ofStringPublisher(payload))
                .header("Content-Type", "application/json")
                .uri(URI("http://localhost:8081/api/stores/1"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.NO_CONTENT_204, response.statusCode())
    }

    @Test
    fun `when patching a store returns 404 when store does not exist`() {
        val payload = """
            {
                "op": "/name",
                "path": "replace",
                "value": "store"
            }
        """

        val response = newHttpClient().send(
            newBuilder()
                .method("PATCH", ofStringPublisher(payload))
                .header("Content-Type", "application/json")
                .uri(URI("http://localhost:8081/api/stores/2"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.NOT_FOUND_404, response.statusCode())
    }

    @Test
    fun `when patching a store returns 400 when patch request is invalid`() {
        val payload = """
            {
                "w0t": "/name",
                "what": "replace",
                "wat": "store"
            }
        """

        val response = newHttpClient().send(
            newBuilder()
                .method("PATCH", ofStringPublisher(payload))
                .header("Content-Type", "application/json")
                .uri(URI("http://localhost:8081/api/stores/1"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.BAD_REQUEST_400, response.statusCode())
    }

    @Test
    fun `when export csv returns 200 and stores as csv`() {
        val response = newHttpClient().send(
            newBuilder()
                .GET()
                .uri(URI("http://localhost:8081/api/stores/export.csv"))
                .build(),
            ofStringHandler()
        )

        assertEquals(HttpStatus.OK_200, response.statusCode())

        assertEquals(
            """
            id,type,name,code,openingDate,description,seasons,specialFields
            1,Other,name,code,2021-03-06,description,[],[]
            """.trimIndent()
                .replace("\n", "\r\n"),
            response.body().trim()
        )
    }
}
