package adapters.repositories.pg

import domain.models.StoreDetails
import domain.ports.repositories.StoreRepository
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import org.testcontainers.containers.PostgreSQLContainer
import kotlin.test.assertEquals
import kotlin.test.assertNull

class StoreRepositoryTest {

    private val postgres = PostgreSQLContainer<Nothing>("postgres").apply {
        withDatabaseName("acme")
        withExposedPorts(5434)
        withUsername("y")
        withPassword("z")
    }

    private val repository: StoreRepository = run {
        postgres.start()
        StoreRepository(
            Database.connect(
                driver = "org.postgresql.Driver",
                url = postgres.jdbcUrl,
                user = postgres.username,
                password = postgres.password,
            )
        )
    }

    @Test
    fun `given storeDetails when save successfully inserts store`() {
        val storeDetails = StoreDetails(123, "code", "description", "name", "2021-03-05", "OTHER")

        repository.save(storeDetails)

        val stores = repository.getAll(1, null)

        assertEquals(1, stores.size)
        assertEquals(123, stores.single().externalId)
    }

    @Test
    fun `given storeDetails when save successfully updates store if already exists`() {
        val first = StoreDetails(1234, "code", "description", "name", "2021-03-05", "OTHER")
        repository.save(first)

        val stores = repository.getAll(1, null)

        assertEquals(1, stores.size)
        assertEquals("code", stores.single().code)

        val second = StoreDetails(1234, "code123", "description", "name", "2021-03-05", "OTHER")
        repository.save(second)

        val updated = repository.getAll(1, null)

        assertEquals(1, updated.size)
        assertEquals("code123", updated.single().code)
    }

    @Test
    fun `given getAll returns all elements that match the query`() {
        val first = StoreDetails(1234, "code", "description", "name", "2021-03-05", "OTHER")
        repository.save(first)

        val second = StoreDetails(12345, "code123", "description", "other", "2021-03-05", "OTHER")
        repository.save(second)

        val all = repository.getAll(1, null)
        val stores = repository.getAll(1, "oth")

        assertEquals(2, all.size)
        assertEquals(1, stores.size)
        assertEquals("other", stores.single().name)
    }

    @Test
    fun `given getAll returns empty if no match`() {
        val first = StoreDetails(1234, "code", "description", "name", "2021-03-05", "OTHER")
        repository.save(first)

        val second = StoreDetails(12345, "code123", "description", "other", "2021-03-05", "OTHER")
        repository.save(second)

        val stores = repository.getAll(1, "random")

        assertEquals(0, stores.size)
    }

    @Test
    fun `given getByExternalId returns the matching store`() {
        val first = StoreDetails(1234, "code", "description", "name", "2021-03-05", "OTHER")
        repository.save(first)

        val second = StoreDetails(12345, "code123", "description", "other", "2021-03-05", "OTHER")
        repository.save(second)

        val store = repository.getByExternalId(12345)

        assertEquals("code123", store!!.code)
        assertEquals(12345, store.externalId)
    }

    @Test
    fun `given getByExternalId returns null if no match`() {
        val first = StoreDetails(1234, "code", "description", "name", "2021-03-05", "OTHER")
        repository.save(first)

        val store = repository.getByExternalId(0)

        assertNull(store)
    }
}
