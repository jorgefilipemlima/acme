package adapters.repositories.pg

import domain.models.StoreDetails
import domain.models.StoreSpecialFieldDetails
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import org.testcontainers.containers.PostgreSQLContainer
import kotlin.test.assertEquals

class StoreSpecialFieldRepositoryTest {
    private val postgres = PostgreSQLContainer<Nothing>("postgres").apply {
        withDatabaseName("acme")
        withExposedPorts(5434)
        withUsername("y")
        withPassword("z")
    }

    private val storeRepository: StoreRepository = run {
        postgres.start()
        StoreRepository(
            Database.connect(
                driver = "org.postgresql.Driver",
                url = postgres.jdbcUrl,
                user = postgres.username,
                password = postgres.password,
            )
        )
    }

    private val storeSpecialFieldRepository: StoreSpecialFieldRepository = run {
        postgres.start()
        StoreSpecialFieldRepository(
            Database.connect(
                driver = "org.postgresql.Driver",
                url = postgres.jdbcUrl,
                user = postgres.username,
                password = postgres.password,
            )
        )
    }

    @Test
    fun `given storeSpecialField when save successfully inserts store special field`() {
        storeRepository.save(StoreDetails(123, "code", "description", "name", "2021-03-05", "OTHER"))
        val existingStoreId = storeRepository.getAll(1, null).single().id!!

        storeSpecialFieldRepository.save(StoreSpecialFieldDetails(existingStoreId, "name", "value"))

        val storeSpecialField = storeSpecialFieldRepository.getAll().single()

        assertEquals("name", storeSpecialField.name)
        assertEquals("value", storeSpecialField.value)
    }

    @Test
    fun `given storeSpecialField when save successfully updates if store special field already exists`() {
        storeRepository.save(StoreDetails(123, "code", "description", "name", "2021-03-05", "OTHER"))
        val existingStoreId = storeRepository.getAll(1, null).single().id!!

        storeSpecialFieldRepository.save(StoreSpecialFieldDetails(existingStoreId, "name", "value"))
        storeSpecialFieldRepository.save(StoreSpecialFieldDetails(existingStoreId, "name", "newValue"))

        val storeSpecialField = storeSpecialFieldRepository.getAll().single()

        assertEquals("name", storeSpecialField.name)
        assertEquals("newValue", storeSpecialField.value)
    }
}
