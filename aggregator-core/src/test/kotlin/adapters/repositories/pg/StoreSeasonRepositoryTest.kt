package adapters.repositories.pg

import domain.models.StoreDetails
import domain.models.StoreSeasonDetails
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import org.testcontainers.containers.PostgreSQLContainer
import kotlin.test.assertEquals

class StoreSeasonRepositoryTest {
    private val postgres = PostgreSQLContainer<Nothing>("postgres").apply {
        withDatabaseName("acme")
        withExposedPorts(5434)
        withUsername("y")
        withPassword("z")
    }

    private val storeRepository: StoreRepository = run {
        postgres.start()
        StoreRepository(
            Database.connect(
                driver = "org.postgresql.Driver",
                url = postgres.jdbcUrl,
                user = postgres.username,
                password = postgres.password,
            )
        )
    }

    private val storeSeasonRepository: StoreSeasonRepository = run {
        postgres.start()
        StoreSeasonRepository(
            Database.connect(
                driver = "org.postgresql.Driver",
                url = postgres.jdbcUrl,
                user = postgres.username,
                password = postgres.password,
            )
        )
    }

    @Test
    fun `given storeSeasonDetails when save successfully inserts store season`() {
        storeRepository.save(StoreDetails(123, "code", "description", "name", "2021-03-05", "OTHER"))
        val existingStoreId = storeRepository.getAll(1, null).single().id!!

        storeSeasonRepository.save(StoreSeasonDetails(existingStoreId, "2021-03-06", "2021-04-06"))

        val storeSeason = storeSeasonRepository.getAll().single()

        assertEquals("2021-03-06", storeSeason.startDate)
        assertEquals("2021-04-06", storeSeason.endDate)
    }

    @Test
    fun `given storeSeasonDetails when save successfully updates if store season already exists`() {
        storeRepository.save(StoreDetails(123, "code", "description", "name", "2021-03-05", "OTHER"))
        val existingStoreId = storeRepository.getAll(1, null).single().id!!

        storeSeasonRepository.save(StoreSeasonDetails(existingStoreId, "2021-03-06", "2021-04-06"))
        storeSeasonRepository.save(StoreSeasonDetails(existingStoreId, "2021-03-04", "2021-04-06"))

        val storeSeason = storeSeasonRepository.getAll().single()

        assertEquals("2021-03-04", storeSeason.startDate)
        assertEquals("2021-04-06", storeSeason.endDate)
    }
}
