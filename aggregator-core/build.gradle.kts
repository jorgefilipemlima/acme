import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"
    id("com.github.johnrengelman.shadow") version "6.1.0"
    application
}

group = "me.jl"
version = "1.0"

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://dl.bintray.com/kotlin/kotlinx") }
    maven { url = uri("https://dl.bintray.com/kotlin/ktor") }
}

dependencies {
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testImplementation("org.mockito:mockito-core:3.8.0")
    testImplementation("org.mockito:mockito-inline:3.8.0")
    testImplementation("io.ktor:ktor-server-test-host:1.4.0")
    testImplementation("io.ktor:ktor-server-tests:1.4.0")
    testImplementation("org.testcontainers:testcontainers:1.15.2")
    testImplementation("org.testcontainers:postgresql:1.15.2")
    testImplementation("org.skyscreamer:jsonassert:1.5.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
    implementation("io.ktor:ktor-server-core:1.4.0")
    implementation("io.ktor:ktor-server-netty:1.4.0")
    implementation("io.ktor:ktor-serialization:1.4.0")
    implementation("io.ktor:ktor-jackson:1.4.0")
    implementation("org.jetbrains.exposed:exposed:0.17.9")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.lettuce:lettuce-core:6.0.2.RELEASE")
    implementation("org.postgresql:postgresql:42.+")
    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:0.15.1")
    implementation(kotlin("stdlib-jdk8"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClassName = "ServerKt"
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
