# ACME Stores

## Disclaimer
This project was a bit over-engineered, however I took the chance to refresh a few concepts and experiment with Redis Streams which I haven't before.

## To run the Application
- Navigate to the folder `dockerfiles`
- Run `docker-compose up`
- Browse `localhost:8080` (It might take a bit for the stores to load, give it few seconds)

## Tech Stack
- Kotlin
- Vue
- Postgres
- Redis
- Docker

## A few thoughts on some decisions:

####  On Redis Streams:
    
I've decided to use Redis Streams in order to separate the core business from data importing/processing.

However this decision came with a few caveats.
I've initially started by having 3 separate streams, one from each endpoint which would then be consumed by the core in 3 different co-routines. Since everything was running in parallel the first few times consuming would lead to very few seasons/special fields created because the stores were still not created.

I've decided to use a single stream in order to ensure order for the challenge purpose.

## Possible Improvements
- Pipelining:
    - Besides importing data, we could have a separate processing flow into a different stream so that the data that arrives to the core is clean and ready to be consumed (This might be a bit of an overkill though)
- Batch inserts/updates:
    - This was my first approach, however when moving to a single stream I kinda discarded this, which I shouldn't.
    - I could've still, even using a single stream, get all messages of certain type, separate into batches, validate the entries and bulk insert, but due to time constraints I've decided to skip this in order to finish the challenge.
- Testing:
    - It was kinda my bad to decide to play around with new technologies as it had an overhead of me having to learn it, which restricted the time I had left, so I didn't test a few parts of the codebase like:
        - The Worker (although it just acts as a proxy, I could've done some more tests)
        - The UI, yeah, this one I feel bad about because I didn't have the chance to try testing in Vue, I'll make sure I do afterwards.
- Kotlin:
    - To be honest here, the last time I've used KT was over a year ago, and although it was good to remember how great Kotlin is I feel the code could've been more Kotlinish :D 

## Overall Feedback
This was indeed a great challenge, I really enjoyed taking time to do this, kudos for making this as challenging as fun.




